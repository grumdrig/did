did
===

Records and prints things I did and that are TODO. 

The database of such things is a text file at `~/.did`.

Go
--

    $ ln -sf did.py ~/bin/did

    $ did Added did project to repository
    Wed 2013.01.09
    - 08:42 Added did project to repository

    $ did -y
    Nothing for Tue 2013.01.08

    $ did -h

    Keeps track of what I've done gone and did

    USAGE: did.py [OPTS] [Stuff I've done]

    Without args, print what I've done today (or yesterday, if nothing),
    otherwise, add a task to today, and then print.

    If the [Stuff I've done] argument is a single integer, it is treated
    as one of the numbered TODO list items, which is removed from the TODO
    list to the specified day's doings.

    OPTS:
      -y    Pretend it's yesterday (use multiply to back up further)
      -2    Operate on the TODO list rather than a did list
      -1    Operate on the PROJECTS list of big-project TODOs
      -l    Operate on a specified custom list
      -d N  Print N days of history  [1]
      -w    Back up one monday, and default -d to 7
      -e    Edit the .did file
      -a    Print all days
      -q    Dont print
      -s    Supress HH:MM timestamp on added tasks
      -f DB Specify alternate database DB  [~/.did]
      -m MN Rewind time MN minutes back
      -h    What what what?

    FILES: ~/.did  The 'database'...

