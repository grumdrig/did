#!/usr/bin/env python

"""
Keeps track of what I've done gone and did

USAGE: did.py [OPTS] [Stuff I've done]

Without args, print what I've done today (or yesterday, if nothing),
otherwise, add a task to today, and then print.

If the [Stuff I've done] argument is a single integer, it is treated
as one of the numbered TODO list items, which is removed from the TODO
list to the specified day's doings.

OPTS:
  -y    Pretend it's yesterday (use multiply to back up further)
  -2    Operate on the TODO list rather than a did list
  -1    Operate on the PROJECTS list of big-project TODOs
  -l    Operate on a specified custom list
  -d N  Print N days of history  [1]
  -w    Back up one monday, and default -d to 7
  -e    Edit the .did file
  -a    Print all days
  -q    Dont print
  -s    Supress HH:MM timestamp on added tasks
  -f DB Specify alternate database DB  [~/.did]
  -m MN Rewind time MN minutes back
  -h    What what what?

FILES: ~/.did  The 'database'...

If the environment varable TIMEZONE is set, that time zone will be
used instead of local time.
"""


__author__ = "efredricksen@gmail.com (Eric Fredricksen)"


import sys, os, string, getopt, time, re, datetime

ONEDAY = 24*60*60
DIDFILE = os.path.join(os.environ['HOME'], '.did')
TODO = 'TODO'
TIMEZONE = os.environ.get('TIMEZONE')
if TIMEZONE:
  try:
    import pytz
    TIMEZONE = pytz.timezone(TIMEZONE)
  except:
    print >>sys.stderr, "Unknown time zone", TIMEZONE
    sys.exit()

def Dayfmt(day):
  return datetime.datetime.fromtimestamp(day, TIMEZONE).strftime("%a %Y.%m.%d")

def main():
  opts,args = getopt.getopt(sys.argv[1:], '21l:ad:ef:hm:swy')
  days = None
  all = 0
  day = time.time()
  didlist = None
  stamp = True if args and (':' not in args[0]) else None

  for o,a in opts:
    if o == '-y':
      day -= ONEDAY
      stamp = None
    elif o == '-2':
      didlist = TODO
      stamp = None
    elif o == '-1':
      didlist = 'PROJECTS'
      stamp = None
    elif o == '-l':
      didlist = a
    elif o == '-f':
      global DIDFILE
      DIDFILE = a
    elif o == '-d':
      days = int(a)
    elif o == '-w':
      day -= ONEDAY
      while -1 == string.find(Dayfmt(day), 'Mon'):
        day -= ONEDAY
      if days == None: days = 7
      stamp = None
    elif o == '-a':
      all = 1
    elif o == '-e':
      os.system(os.environ['EDITOR'] + ' ' + DIDFILE)
      sys.exit()
    elif o == '-s':
      stamp = None
    elif o == '-m':
      day -= float(a) * 60
    elif o == '-h':
      print __doc__
      sys.exit()
  if days == None: days = 1
  today = didlist or Dayfmt(day)
  if stamp: args.insert(0, datetime.datetime.fromtimestamp(day, TIMEZONE).strftime("%H:%M"))
      
  didtodo = False
  dones = Read(open(DIDFILE))
  if args:
    assert days == 1
    dones.setdefault(today, [])
    task = ' '.join(['-'] + args)
    if re.match(r'^- [1-9][0-9]*$', task):
      # It's a single number (therefore referencing a TODO item)
      index = int(task[2:])
      if len(dones.get(TODO,[])) < index:
        print >>sys.stderr, 'No such', TODO, 'item'
        sys.exit()
      task = dones[TODO].pop(index-1)
      didtodo = True
    dones[today].append(task)
    Write(open(DIDFILE,'w'), dones)
  while days > 0:
    if not Write(sys.stdout, dones, not all and today):
      if not all:
        print 'Nothing for', today
    if not didlist:
      day += ONEDAY
      today = Dayfmt(day)
    else:
      assert days == 1
    days -= 1
  if didtodo and day != TODO and not all:
    Write(sys.stdout, dones, TODO)
    


def Write(donefile, dones, day=None):
  aught = 0
  ks = dones.keys()
  def sbd(x,y):
    [x,y] = [d.split()[1:] for d in [x,y]]
    return (x<y) and -1 or (x>y) and 1 or 0
  ks.sort(sbd)
  for k in ks:
    if k == (day or k):
      aught += 1
      donefile.write(k + '\n')
      for l in dones[k]:
        if l:
          donefile.write(l + '\n')
  return aught


def Read(donefile):
  result = {}
  day = None
  lineno = 0
  for l in donefile.readlines():
    lineno += 1
    if l[0] == '-':
      if not day: Error('Invalid format in line %d: %s' % (lineno,l))
      result.setdefault(day, []).append(l.strip())
    else:
      day = l.strip()
  return result


if __name__ == '__main__':
  main()
